#!/usr/bin/env python2.6
#coding: utf-8

from __future__ import print_function
from __future__ import unicode_literals

__author__ ="bphanzhu@gmail.com"
__version__ = "0.1"

class DataCharBuffer:

    def __init__(self, data=[]):
        self.data = data
        self.length = len(self.data)

class IndexBuffer:

    def __init__(self, ):
        self.positions = [] # 令牌或者元素的起始位置
        self.lengths = [] # 令牌或者元素的长度
        self.types = [] # 令牌或者元素的类型
        self.count = 0 # 令牌或者元素的总个数

# 令牌类型
NGX_BRACKET_LEFT = 0
NGX_BRACKET_RIGHT = 1
NGX_COLON = 2
NGX_COMMENT = 3
NGX_ENC_STR = 4
NGX_STR = 5

class Tokenizer:

    def __init__(self, data_buffer, token_buffer):
        self.data_buffer = data_buffer
        self.token_buffer = token_buffer
        self.data_position = 0
        self.token_index = 0
        self.token_length = 0

    def has_more_token(self):
        return self.data_position + self.token_length + 1 < self.data_buffer.length

    def parse_token(self):
        self.skip_blank_space()
        self.token_buffer.positions.insert(self.token_index, self.data_position) # 记录 token 起始位置
        next_ch = self.data_buffer.data[self.data_position] # 开始解析 token
        if next_ch == "{":
            self.token_buffer.types.insert(self.token_index, NGX_BRACKET_LEFT) # 记录 token 类型
            self.token_length = 1
        elif next_ch == "}":
            self.token_buffer.types.insert(self.token_index, NGX_BRACKET_RIGHT)
            self.token_length = 1
        elif next_ch == ";":
            self.token_buffer.types.insert(self.token_index, NGX_COLON)
            self.token_length = 1
        elif next_ch == "#":
            self.token_buffer.types.insert(self.token_index, NGX_COMMENT)
            self.token_length = 1
        else:
            self.parse_str()
        self.token_buffer.lengths.insert(self.token_index, self.token_length) # 记录 token 长度

    def skip_blank_space(self):
        is_last_blank_space = True
        while (is_last_blank_space):
            ch = self.data_buffer.data[self.data_position]
            if ch in (' ', '\r', '\n', '\t'):
                self.data_position += 1
            else:
                is_last_blank_space = False

    def parse_str(self):
        temp_pos = self.data_position
        contains_encoded_chars = False
        end_of_string_found = False
        while not end_of_string_found:
            temp_pos += 1
            ch = self.data_buffer.data[temp_pos]
            if ch == '\\':
                contains_encoded_chars = True
            elif ch in [' ', '\r', '\n', '\t', '#', ';', '{', '}']:
                last_ch = self.data_buffer.data[temp_pos -1]
                end_of_string_found = last_ch != '\\'

        if contains_encoded_chars:
            self.token_buffer.types.insert(self.token_index, NGX_ENC_STR)
        else:
            self.token_buffer.types.insert(self.token_index, NGX_STR)

        self.token_buffer.positions.insert(self.token_index,self.data_position + 1)
        self.token_length = temp_pos - self.data_position

    def next_token(self):
        if len(self.token_buffer.lengths) != 0:
            self.data_position += self.token_buffer.lengths[self.token_index]
            self.token_index += 1

    def token_type(self):
        self.token_buffer.types[self.token_index]

DEFAULT_INDENT = 4

class Context(object):
    def __init__(self, name):
        self.name = name
        self.directives = []

    def add_directive(self, directive):
        self.directives.append(directive)

class Directive(object):

    def __init__(self, name):
        self.name = name
        self.params = [];
        self.block = None;

class Comment(Directive):

    def __init__(self):
        self.name = "comment"

    def set_content(self, content):
        self.params.append(content)

    def get_content(self):
        if len(self.params):
            return self.params[0]
        return ""

DIRECTIVE_START = 0;
DIRECTIVE_PARAM = 1;
DIRECTIVE_END = 2;
BLOCK_START = 3;
BLOCK_END = 4;
COMMENT_START = 5;
COMMENT_END = 6;

class SyntaxException(Expception):
    pass

class Parser:

    def __init__(self, token_buffer, element_buffer):
        self.token_buffer = token_buffer
        self.element_buffer = element_buffer

    def parse(self, data_buffer, ctx = None):
        self.element_index = 0
        self.ctx = ctx if ctx else Context("main")
        self.tokenizer = Tokenizer(data_buffer, self.token_buffer)
        self.parse_directive(self.tokenizer, self.ctx)
        self.element_buffer.count = self.element_index

    def parse_directive(self, tokenizer, ctx):
        if tokenizer.has_more_token():
            tokenizer.next_token()
            tokenizer.parse_token()

            token_type = tokenizer.token_type()
            self.assert_token_type(NGX_STR, token_type)
            set_element_data(tokenizer, DIRECTIVE_START)
            
            tokenizer.next_token()
            tokenizer.parse_token()
            token_type = tokenizer.token_type()
            while token_type != NGX_BRACKET_RIGHT or token_type != NGX_COLON:
                # 检查 token_type 类型？
                if token_type == 

    def peek_ctx(self):
        if len(self.ctxs) > 0:
            return self.ctxs[len(self.ctxs) - 1]

    def assert_has_more_tokens(self, tokenizer):
        if not tokenizer.has_more_token():
            raise SyntaxException()

    def assert_token_type(self, expect_token_type, tokey_type):
        if expect_token_type != token_type:
            raise SyntaxException("unexpected token_type=" + token_type)

    def set_element_data(self, tokenizer, element_type):
        self.element_buffer.positions.insert(self.element_index, tokenizer.token_position())
        self.element_buffer.lengths.insert(self.element_index, tokenizer.token_length())
        self.element_buffer.types.insert(self.element_index, element_type)
        self.element_index += 1

def load(file):
    pass

def main():
    return 0

if __name__ == "__main__":
    import sys
    sys.exit(main())

